# Server Setup

Initial setup

cd ~/.local/lib

git clone https://gitlab.com/chrishunter/server-setup.git

ln -s ~/.local/lib/server-setup/build ~/.local/bin/build-env

-- Done up to here

##

# Setup NVIDIA transcoding

```
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
sudo systemctl restart docker
sudo apt-get install nvidia-container-runtime

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo tee /etc/systemd/system/docker.service.d/override.conf <<EOF
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd --host=fd:// --add-runtime=nvidia=/usr/bin/nvidia-container-runtime
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

Now `runtime: nvidia` will work.

## Install "Setup Mount points", and mount ZFS with fstab
## Clone mediaserver config
## Init docker-compose

### OLD instructions

First clone this repo into `/srv/nas/config`:

    git clone git@gitlab.com:chrishunter/mediaserver.git

# ZFS

Creating a new zfs raidz is done by:

    zpool create -f nas raidz /dev/vdb /dev/vdc /dev/vdd

And you can set the mount point, if it is in the wrong place, like so:

    zfs set mountpoint=[MOUNT POINT] [ZPOOL NAME]
    or for a dataset 
    fs create -o mountpoint=[MOUNT POINT] [ZPOOL NAME]/[DATASET NAME]

Sources: https://www.jamescoyle.net/how-to/478-create-a-zfs-volume-on-ubuntu

# Mergerfs

https://github.com/trapexit/mergerfs

Install the latest version from mergerfs site https://github.com/trapexit/mergerfs/releases

Update fstab with the following:

    /mnt/raid/*  /srv/nas  fuse.mergerfs  defaults,allow_other,use_ino,minfreespace=4G,dropcacheonclose=true,category.create=mfs,func.getattr=newest       0       0


# Docker / Podman

# Nginx

# Emby

# Transmission

