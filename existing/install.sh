sudo apt-get update

sudo apt-get install git curl jq unrar
sudo snap install bw

bw login # TODO: Only do this if not already logged in
bw unlock

# TODO: Need to capture the session token from the unlock

# TODO: Install SSH cert from Bitwarden


# Install Docker
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker

sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
    
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker

# Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# MergerFS
curl -OL https://github.com/trapexit/mergerfs/releases/download/2.32.2/mergerfs_2.32.2.ubuntu-focal_amd64.deb

sudo dpkg -i mergerfs_2.32.2.ubuntu-focal_amd64.deb

# Smart disk names
cd ~/Projects
git clone git@gitlab.com:chrishunter/server-setup.git
sudo apt install smartmontools
sudo cp ./smart_disk_naming/90-smart-persistent-storage.rules /etc/udev/rules.d
sudo cp ./smart_disk_naming/smart_details /usr/local/bin/
sudo chmod +x /usr/local/bin/smart_details

# Setup ZFS

sudo apt install zfsutils-linux
sudo zpool import -f cZhahafa
sudo zpool import -f nas1

# TODO: Enable bi-weekly scrubs

# TODO: Enable MergerFS

echo "/mnt/raid/*  /srv/nas  fuse.mergerfs  defaults,allow_other,use_ino,minfreespace=4G,dropcacheonclose=true,category.create=mfs,func.getattr=newest       0       0" >> /etc/fstab
