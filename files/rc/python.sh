###
# PYTHON SPECIFIC CONFIG
###

# dont create .pyc files
export PYTHONDONTWRITEBYTECODE=1

# python virtualenvwrapper config
export PROJECT_HOME=.

# Ansible config override (prefer local to home) (see: https://docs.ansible.com/ansible/2.4/intro_configuration.html)
export ANSIBLE_CONFIG=./ansible.cfg:~/.ansible.cfg

# corrects a bug in High Sierra 10.13.x for processes that use fork()
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES

# switch environment stored credentials for ansible vault using predefined labels
# expects ~/.ansible-vault-map to exist with using yaml syntax to define label:path|password
# deeper structures permitted (use '.' syntax with $1 to indicate deeper structures)
# Args:
#   $1: label to find vault password or path to file (optional - if not provided, unsets existing envars)
vault-switch() {
    local LABEL=$1
    local KEY="vault_config_${LABEL/./_}" # since the parse_yaml returns deep keys using '_' separators
    local MAP_PATH="${HOME}/.ansible-vault-map"
    local ANSIBLE_CONFIG_PATH="${HOME}/.ansible.cfg"

    if [ "${1}" == "--create" ]; then
        if [ -f "${MAP_PATH}" ]; then
            echo "${MAP_PATH} already exists"
            return 1
        fi

        echo "# enter mappings as yaml, e.g\n# label: absolute-path (or password), e.g.\n#prod: \$HOME/.ansible-vault/aws-prod" >! $MAP_PATH
        echo "${MAP_PATH} created. Please update the file with your ansible vault passwords/password file paths"
        return
    fi

    if ! [ -f "${MAP_PATH}" ]; then
        echo "${MAP_PATH} not found. Create new yaml file with 'vault-switch --create'"
        return 1
    fi

    if [ ! "${LABEL}" ]; then
        unset ANSIBLE_VAULT_PASSWORD
        unset ANSIBLE_VAULT_PASSWORD_FILE
        unset ANSIBLE_VAULT_PASSWORD_LABEL
        echo "Unset Ansible Vault environment variables"
        return
    fi

    eval $(parse_yaml ${MAP_PATH} "vault_config_")

    # bit of zsh magic to use dynamic variable names
    local ANSIBLE_VAULT_PW=${(P)KEY}

    if [ ! ${ANSIBLE_VAULT_PW} ]; then
        echo "${LABEL} not found in ${MAP_PATH}"
        return 1
    fi

    if [ -f "${ANSIBLE_VAULT_PW}" ]; then
        export ANSIBLE_VAULT_PASSWORD_FILE=${ANSIBLE_VAULT_PW}
        # replacing just the env is helpful, but many editors will still prefer to look at the ansible.cfg file
        if [ -f "${ANSIBLE_CONFIG_PATH}" ]; then
            sed -i -E "s:vault_password_file =.*:vault_password_file = ${ANSIBLE_VAULT_PW}:" ${ANSIBLE_CONFIG_PATH}
        fi
    else
        export ANSIBLE_VAULT_PASSWORD=${ANSIBLE_VAULT_PW}
    fi

    export ANSIBLE_VAULT_PASSWORD_LABEL=${LABEL}
    echo "Success! '${LABEL}' vault password loaded in environment"
}
