# similar to .zshrc-custom, but config that you specifically want to add AFTER the installer-managed-code
if [ -f "${HOME}/.zshrc-custom-append" ]; then
    source "${HOME}/.zshrc-custom-append"
fi
