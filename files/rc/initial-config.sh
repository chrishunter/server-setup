################################
# Start of Environment Installer
# ------------------------------
# Please do not replace any of the #START {x} or #END {x}
# comments as these are needed by the environment installer
#
# It's recommended to not change content between the START/END
# markers as this would be replaced by any future use of the
# Installer (for updates etc).
################################

# Although not required, its suggested to use .zshrc-custom for
# any manual configuration as this will allow you to completely
# destroy your zsh environment and recreate from scratch
if [ -f "${HOME}/.zshrc-custom" ]; then
    source "${HOME}/.zshrc-custom"
fi

###
# ZSH POWERLEVEL 9/10K THEME
###
POWERLEVEL9K_MODE='nerdfont-complete'
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_to_last"
POWERLEVEL9K_FOLDER_ICON='\uF07C'
POWERLEVEL9K_TIME_BACKGROUND="clear"
POWERLEVEL9K_TIME_FOREGROUND="cyan"
POWERLEVEL9K_VCS_CLEAN_FOREGROUND='black'
POWERLEVEL9K_VCS_CLEAN_BACKGROUND='green'
POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND='black'
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='yellow'
POWERLEVEL9K_VCS_MODIFIED_FOREGROUND='black'
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='yellow'
POWERLEVEL9K_RIGHT_SEGMENT_SEPARATOR=''
POWERLEVEL9K_RIGHT_SUBSEGMENT_SEPARATOR=''
POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND='clear'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND='blue'
POWERLEVEL9K_STATUS_CROSS=true
POWERLEVEL9K_STATUS_OK=false
POWERLEVEL9K_COMMAND_EXECUTION_TIME_THRESHOLD=0
POWERLEVEL9K_VCS_INCOMING_CHANGES_ICON='\u2193'
POWERLEVEL9K_VCS_OUTGOING_CHANGES_ICON='\u2191'
POWERLEVEL9K_VCS_GIT_ICON='\uF1D3'
POWERLEVEL9K_VCS_GIT_GITHUB_ICON='\uF113'
POWERLEVEL9K_VCS_COMMIT_ICON="\uf417"
POWERLEVEL9K_APPLE_ICON='\uF179'
POWERLEVEL9K_ROOT_ICON='\uE614'
POWERLEVEL9K_PYTHON_ICON='\uf81f'
POWERLEVEL9K_LEFT_SUBSEGMENT_SEPARATOR='\ue621'
POWERLEVEL9K_VAULT_ICON='\uf023'

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(root_indicator os_icon ssh dir_writable dir python_virtualenv aws_role vault_info vcs) #
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(command_execution_time status time)
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="%F{blue}\u256D\u2500%F{white}"
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%F{blue}\u2570\uf105%F{white} "

prompt_python_virtualenv() {
    local virtualenv_path="$VIRTUAL_ENV"
    if [[ -n "$virtualenv_path" && "$VIRTUAL_ENV_DISABLE_PROMPT" != true ]]; then
        if [ "${PWD##*/}" = "${virtualenv_path##*/}" ]; then
            p10k segment -b "white" -f "black" -i "PYTHON_ICON" -r -s 0 -t ""
        else
            p10k segment -b "white" -f "black" -i "PYTHON_ICON" -r -s 0 -t "$(basename "$virtualenv_path")"
        fi
    fi
}

prompt_vault_info() {
    if ([[ "$ANSIBLE_VAULT_PASSWORD_FILE" ]] || [[ "$ANSIBLE_VAULT_PASSWORD" ]]) && [[ "$ANSIBLE_VAULT_PASSWORD_LABEL" ]]; then
        # "$1_prompt_segment" "$0" "$2" 242 253 "VAULT_ICON" 0 "" "${ANSIBLE_VAULT_PASSWORD_LABEL}" #powerlevel10k
        "$1_prompt_segment" "$0" "$2" 242 253 "${ANSIBLE_VAULT_PASSWORD_LABEL}" "VAULT_ICON" #powerlevel9k
    fi
}

prompt_aws_role() {
    AWS_ROLE=
    if [[ "$AWS_ACCOUNT_NAME" && "$AWS_ACCOUNT_ROLE" ]]; then
        PARENT_ACCOUNT=${AWSUME_PROFILE:-"default"}
        if [[ "$PARENT_ACCOUNT" == "default" ]]; then
            AWS_ROLE="${AWS_ACCOUNT_NAME}:${AWS_ACCOUNT_ROLE}"
        else
            AWS_ROLE="${PARENT_ACCOUNT}.${AWS_ACCOUNT_NAME}:${AWS_ACCOUNT_ROLE}"
        fi
    elif [[ "$AWSUME_PROFILE" && "$AWS_ACCESS_KEY_ID" && "$AWS_SECRET_ACCESS_KEY" ]]; then
        AWS_ROLE="${AWSUME_PROFILE}"
    fi

    if [ "$AWS_ROLE" ]; then
        p10k segment -b 166 -f "black" -i "AWS_ICON" -r -s 0 -t "${AWS_ROLE}"
        # "$1_prompt_segment" "$0" "$2" 166 "black" "${AWS_ROLE}" "AWS_ICON" #powerlevel9k
    fi
}

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
    source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

alias reload=". ${HOME}/.zshrc && echo 'ZSH config reloaded from ${HOME}/.zshrc'"
# seems to correct a problem with zsh and curl where curl trys to glob any text
alias curl='noglob curl'

# because zsh nomatch error reporting is a bit intrusive
unsetopt nomatch

# copies an already defined function (to allow us to override the original)
# Args:
#   $1: original function name
#   $2: new function name
copy_function() {
    local ORIG_FUNC=$(declare -f $1)
    local NEWNAME_FUNC="$2${ORIG_FUNC#$1}"
    eval "$NEWNAME_FUNC"
}

# Simple yaml parser to convert yaml template into shell variables
# Keys in deeper structures will combined its parent key with "_"
# Args:
#   $1: file to parse
#   $2: prefix to prepend to all variable names
parse_yaml() {
    local prefix=$2
    local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @ | tr @ '\034')
    sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p" $1 |
        awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

# iterm2 profile switcher
# Args:
#   $1: name of iTerm profile to switch to (case sensitive)
it2prof() {
    echo -e "\033]50;SetProfile=$1\a"
    clear
    echo "iTerm profile loaded: $1"
}
