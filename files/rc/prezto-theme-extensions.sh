###
# ZSH PARADOX THEME
###
function aws_account_info {
  if [[ -n "$AWS_ACCOUNT_NAME" && -n "$AWS_ACCOUNT_ROLE" ]]; then
    PARENT_ACCOUNT=${AWSUME_PROFILE:-"default"}
    prompt_paradox_start_segment 166 black
    if [[ "$PARENT_ACCOUNT" == "default" ]]; then
      prompt_paradox_start_segment 166 black "aws::${AWS_ACCOUNT_NAME}:${AWS_ACCOUNT_ROLE}"
    else
      prompt_paradox_start_segment 166 black "aws::${PARENT_ACCOUNT}.${AWS_ACCOUNT_NAME}:${AWS_ACCOUNT_ROLE}"
    fi
  elif [[ -n "$AWSUME_PROFILE" && -n "$AWS_ACCESS_KEY_ID" && -n "$AWS_SECRET_ACCESS_KEY" ]]; then
    prompt_paradox_start_segment 166 black "aws::${AWSUME_PROFILE}"
  fi
}

function ansible_vault_info {
  if ([[ -n "$ANSIBLE_VAULT_PASSWORD_FILE" ]] || [[ -n "$ANSIBLE_VAULT_PASSWORD" ]]) && [[ -n "$ANSIBLE_VAULT_PASSWORD_LABEL" ]]; then
    prompt_paradox_start_segment 242 253 "\uf023:${ANSIBLE_VAULT_PASSWORD_LABEL}"
  fi
}

function prompt_paradox_build_prompt {
  prompt_paradox_start_segment black default '%(?::%F{red}✘ )%(!:%F{yellow}⚡ :)%(1j:%F{cyan}⚙ :)%F{blue}%n%F{red}@%F{green}%m%f'
  prompt_paradox_start_segment blue black '$_prompt_paradox_pwd'
  if [[ -n "$git_info" ]]; then
    prompt_paradox_start_segment green black '${(e)git_info[ref]}${(e)git_info[status]}'
  fi
  if [[ -n "$python_info" ]]; then
    prompt_paradox_start_segment white black '${(e)python_info[virtualenv]}'
  fi
  aws_account_info
  ansible_vault_info
  prompt_paradox_end_segment
}
